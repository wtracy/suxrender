# suxrender

A toy raytracer. It sux.

Resources:

* https://www.scratchapixel.com/
* http://www.pbr-book.org/

## Todo

* Meshes not working at all
  * [Reference](https://www.scratchapixel.com/lessons/3d-basic-rendering/ray-tracing-rendering-a-triangle/ray-triangle-intersection-geometric-solution)
* GI
* Refraction
* Outlining/stylized rendering

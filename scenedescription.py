from textures import *
from shapes import *


lightDir = np.array([1., 1., .3])
lightColor = [1, 1, 1]

print('Loading geometry...')
shapes = [
  #Sphere((1.5, -0.5, -11), 2),
  #Sphere((-2., -1.5, -12.), 1),
  Mesh('bunny.stl'),
  Plane((0, -2.5, 0), (0, 1, 0)),
]
textures = [
  #Solid((0.05, 0.05, .05), reflectivity=0.7),
  Checkered((1, 0.05, 0.05), (1, 1, 1), reflectivity=0.0),
  Checkered((0, .70, 0), (0, 0.35, 0), reflectivity=0.0),
]

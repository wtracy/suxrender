import numpy as np

from constants import *

class Solid:
  def __init__(self, color, emission=DEFAULT_AMBIENT, diffuse=1, reflectivity=0):
    self.color = color
    self.reflectivity = reflectivity
    self.emission = emission
    self.diffuse = diffuse

  def paint(self, pixels, mask, intersections, normals, textureMasks):
    for row in range(HEIGHT):
      for column in range(WIDTH):
        if mask[row,column]:
          pixels[row,column,:] = self.color
          textureMasks[row, column, REFLECTIVITY] = self.reflectivity
          textureMasks[row, column, EMISSION] = self.emission
          textureMasks[row, column, DIFFUSE] = self.diffuse

class Checkered:
  def __init__(self, c0, c1, reflectivity=0, emission=DEFAULT_AMBIENT, diffuse=1):
    self.c0 = c0
    self.c1 = c1
    self.reflectivity = reflectivity
    self.emission = emission
    self.diffuse = diffuse

  def paint(self, pixels, mask, intersections, normals, textureMasks):
    truncated = np.abs(np.floor(intersections))
    summed = truncated[:,:,0] + truncated[:,:,1] + truncated[:,:,2]
    which = summed % 2
    for row in range(HEIGHT):
      for column in range(WIDTH):
        if mask[row,column]:
          pixels[row,column,:] = self.c0 if which[row, column] else self.c1
          textureMasks[row, column, REFLECTIVITY] = self.reflectivity
          textureMasks[row, column, EMISSION] = self.emission
          textureMasks[row, column, DIFFUSE] = self.diffuse




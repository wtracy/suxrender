from math import sqrt

import numpy as np
from PIL import Image

from constants import *
from vectormath import *
from scenedescription import shapes, textures, lightDir


def main():
  print('Allocating memory...')
  pixels = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  mask = np.zeros([HEIGHT, WIDTH], dtype=np.bool_)
  intersections = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  normals = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  reflectionNormals = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  origins = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  textureMasks = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  reflections = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  reflectionIntersections = np.zeros([HEIGHT, WIDTH, 3])
  reflectionTextureMasks = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
  distances = np.full([HEIGHT, WIDTH], 10000.0)
  reflectionDistances = np.full([HEIGHT, WIDTH], 10000.0)
  origins[:,:,0] = -255

  print('Constructing line-of-sight vectors...')
  vectors = buildLineOfSightVectors()

  print('Rendering initial pass...')
  renderBackground(vectors, pixels, textureMasks)
  render(
      shapes, textures, 
      pixels, origins, vectors, intersections, normals, textureMasks, distances)

  if REFLECTIONS_ON:
    print('Computing reflection vectors...')
    vectorNormalDots = arrayDotArray(normals, vectors).reshape([HEIGHT,WIDTH,1])
    reflectionVectors = vectors - 2 * vectorNormalDots * normals

    print('Rendering reflection pass...')
    renderBackground(reflectionVectors, reflections, reflectionTextureMasks)
    render(
      shapes, textures, 
      reflections, 
      intersections, 
      reflectionVectors, 
      reflectionIntersections,
      reflectionNormals,
      reflectionTextureMasks,
      reflectionDistances)

  print('Encoding and saving images...')
  reflectionPixels = reflections * textureMasks[:,:,REFLECTIVITY:REFLECTIVITY+1]
  pixels = pixels + reflectionPixels
  gammaCompress(pixels)
  writeImageFromFloats(pixels, 255, 'image.png', clamped=True)

  writeNormalizedImage(normals, 'normals.png')
  writeNormalizedImage(reflectionNormals, 'reflectionnormals.png')
  writeNormalizedImage(reflectionVectors, 'reflectionvectors.png')
  writeImageFromFloats(textureMasks, 255, 'texturemasks.png')
  writeImageFromFloats(reflectionTextureMasks, 255, 'reflectiontexturemasks.png')
  gammaCompress(reflections)
  writeImageFromFloats(reflections, 255, 'reflections.png', clamped=True)
  writeImageFromSignedFloats(intersections, 256, 'intersections.png')
  writeImageFromSignedFloats(
    reflectionIntersections, 256, 'reflectionintersections.png')
  writeImageFromFloats(distances, 16., 'depthmap.png', clamped=True)
  writeImageFromFloats(reflectionDistances, 16., 'reflectiondepthmap.png', clamped=True)

def gammaCompress(pixels):
  return np.power(pixels, 1/GAMMA, out=pixels)

def getChannel(mask, channel):
  return mask[:,:,channel:channel+1]

def buildLineOfSightVectors():
  vectors = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)

  for column in range(WIDTH):
    for row in range(HEIGHT):
      vectors[row][column][0]=column
      vectors[row][column][1]=row
      vectors[row][column][2]=-1

# Normalize x elements to the range [-1/3, 1/3], then adjust for viewing ratio
  vectors[:,:,0] = (vectors[:,:,0]/(WIDTH-1) - 0.5) * 2/3 * RATIO
# Normalize y elements to the range [-1/3, 1/3]
  vectors[:,:,1] = (vectors[:,:,1]/(HEIGHT-1) - 0.5) * -2/3

  vectors = normalize2dArrayOfVectors(vectors)
  return vectors

def buildShadingMap(normals):
  lightLength = sqrt(
    lightDir[0]*
    lightDir[0]+
    lightDir[1]*lightDir[1]+
    lightDir[2]*lightDir[2])
  np.divide(lightDir, lightLength, out=lightDir)
  result = dotArray(lightDir, normals)
  result = np.maximum(result, 0)
  return result

def renderBackground(vectors, pixels, textureMasks):
  pixels[:,:,1] = (vectors[:,:,1] - 1) * -0.6# - 0.1
  pixels[:,:,2] = (vectors[:,:,1] - 1) * -0.2 + 0.80
  textureMasks[:,:,EMISSION] = 1
  np.maximum(pixels, 0, out=pixels)
  #print(pixels)

def render(
    shapes, textures, 
    pixels, origins, vectors, intersections, normals, textureMasks, distances):
  mask = np.zeros([HEIGHT, WIDTH], dtype=np.bool_)
  tempD = np.zeros([HEIGHT, WIDTH])
  for shape, texture in zip(shapes, textures):
    shape.intersects(origins, vectors, mask, tempD)
    np.logical_and(mask, tempD < distances, out=mask)
    np.copyto(distances, tempD, where=mask)
    writeIntersections(origins, vectors, mask, distances, intersections)
    shape.writeNormals(intersections, mask, normals)
    texture.paint(pixels, mask, intersections, normals, textureMasks)

  shadingMap = buildShadingMap(normals)
  lightVectors = np.zeros([HEIGHT,WIDTH,3])
  lightVectors[:,:,0] = lightDir[0]
  lightVectors[:,:,1] = lightDir[1]
  lightVectors[:,:,2] = lightDir[2]
  for shape in shapes:
    shape.intersects(intersections, lightVectors, mask, tempD)
    for r in range(HEIGHT):
      for c in range(WIDTH):
        if mask[r,c]:
          shadingMap[r,c] = 0

  np.add(
    pixels * shadingMap.reshape([HEIGHT,WIDTH,1]) * getChannel(textureMasks, DIFFUSE),
    pixels * getChannel(textureMasks, EMISSION),
    out=pixels)

def writeIntersections(origins, vectors, mask, distances, intersections):
  vectorOffsets = vectors * distances.reshape([HEIGHT,WIDTH,1])
  np.add(
    origins, vectorOffsets, 
    where=mask.reshape([HEIGHT,WIDTH,1]), out=intersections)

def writeNormalizedImage(raster, filename):
  writeImageFromFloats(raster + 1, 127, filename)

def writeImageFromSignedFloats(raster, multiplier, filename):
  writeImageFromFloats(np.abs(raster), multiplier, filename)

def writeImageFromFloats(raster, multiplier, filename, clamped=False):
  raster = raster * multiplier
  if clamped:
    np.clip(raster, 0, 255, out=raster)
  raster = raster.astype(np.uint8)
  writeImage(raster, filename)

def writeImage(raster, filename):
  img = Image.fromarray(raster)
  img.save(filename)

if __name__ == '__main__':
  main()

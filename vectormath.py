import numpy as np
import numpy.ma as ma

from constants import *

def normalize2dArrayOfVectors(vectors):
  lengths = np.sqrt(np.sum(vectors**2, axis=-1))
  reciprocals = np.reciprocal(lengths).reshape([HEIGHT, WIDTH, 1])
  np.multiply(reciprocals, vectors, out=vectors)
  return vectors

def dotArray(v, rays):
  return v[0]*rays[:,:,0] + v[1]*rays[:,:,1] + v[2]*rays[:,:,2]

def dot(a, b):
  return a[0]*b[0] + a[1]*b[1] + a[2]*b[2]

def arrayDotArray(a, b):
  return a[:,:,0]*b[:,:,0] + a[:,:,1]*b[:,:,1] + a[:,:,2]*b[:,:,2]

def solveQuadratic(a, b, c, exists, x0, x1):
  discr = np.multiply(b, b) - 4*np.multiply(a, c)
  discr = ma.masked_less(discr, 0)
  np.logical_not(discr.mask, out=exists)

  root = ma.sqrt(discr)
  q_positive = -0.5 * (b + root)
  q_negative = -0.5 * (b - root)
  q = np.where(np.greater(b, np.zeros(a.shape)), q_positive, q_negative)

  c0 = q / a
  c1 = c / q

  np.minimum(c0, c1, out=x0)
  np.maximum(c0, c1, out=x1)



import numpy as np

from stl import mesh

from vectormath import *

class Mesh:
  def __init__(self, source):
    self.mesh = mesh.Mesh.from_file(source)

  def intersectsPlane(self, p0, normal, origins, rays, mask, distances):
    newIntersections = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
    pRelative = p0 - origins
    self.writeDistances(normal, pRelative, rays, distances)

    np.greater(distances, 1e-4, out=mask)

  def intersects(self, origins, rays, mask, distances):
    edge0 = self.mesh.v1 - self.mesh.v0
    edge1 = self.mesh.v2 - self.mesh.v1
    edge2 = self.mesh.v0 - self.mesh.v2

    for i in range(len(edge0)):
      self.intersectsPlane(
        self.mesh.v0[i],
        self.mesh.normals[i],
        origins, rays, mask, distances)
      P = rays * distances.reshape([HEIGHT,WIDTH,1]) + origins

      C0 = P - self.mesh.v0[i]
      C1 = P - self.mesh.v1[i]
      C2 = P - self.mesh.v2[i]
      N = self.mesh.normals[i]
      cross0 = np.cross(edge0[i], C0)
      cross1 = np.cross(edge1[i], C1)
      cross2 = np.cross(edge1[i], C2)
      dot0 = dotArray(N, cross0)
      dot1 = dotArray(N, cross1)
      dot2 = dotArray(N, cross2)

      newMask = np.logical_and(
          np.logical_and(dot0 > 0,
            dot1 > 0),
          dot2 > 0)
      np.logical_or(mask, newMask, out=mask)

  def writeDistances(self, normal, pRelative, rays, distances):
    numerators = dotArray(normal, pRelative)
    denominators = dotArray(normal, rays)
    mask = np.abs(denominators) > 1e-4

    return np.divide(numerators, denominators, out=distances, where=mask)

  def writeIntersections(
      self, origins, rays, mask, distances, intersections):
    pass

  def writeNormals(self, intersections, mask, normal):
    pass

class Plane:
  def __init__(self, point, normal):
    self.p0 = point
    self.normal = normal

  def writeDistances(self, pRelative, rays, distances):
    numerators = dotArray(self.normal, pRelative)
    denominators = dotArray(self.normal, rays)
    mask = np.abs(denominators) > 1e-4

    return np.divide(numerators, denominators, out=distances, where=mask)

  def writeIntersections(
      self, origins, rays, mask, distances, intersections):
    i = np.multiply(rays, distances.reshape([HEIGHT, WIDTH, 1]))
    np.add(i, origins, out=intersections, where=mask.reshape([HEIGHT,WIDTH,1]))

  def writeNormals(self, intersections, mask, normal):
    for r in range(HEIGHT):
      for c in range(WIDTH):
        if mask[r,c]:
          normal[r,c] = self.normal

  def intersects(self, origins, rays, mask, distances):
    newIntersections = np.zeros([HEIGHT, WIDTH, 3], dtype=np.double)
    pRelative = self.p0 - origins
    self.writeDistances(pRelative, rays, distances)

    np.greater(distances, 1e-4, out=mask)


class Sphere:
  def __init__(self, center, radius):
    self.center = np.array(center)
    self.rsquared = radius * radius

  def writeDistances(self, cRelative, rays, mask, distances):
    l = -cRelative
    a = arrayDotArray(rays, rays)
    b = 2 * arrayDotArray(l, rays)
    c = arrayDotArray(l, l) - self.rsquared
    solveQuadratic(a, b, c, mask, distances, None)


  def writeIntersections(self, origins, rays, mask, distances, intersections):
    reshapedMask = mask.reshape([HEIGHT,WIDTH,1])
    distances.reshape([HEIGHT,WIDTH,1])
    np.multiply(rays, distances.reshape([HEIGHT,WIDTH,1]), out=intersections, where=reshapedMask)
    np.add(intersections, origins, out=intersections, where=reshapedMask)

  def writeNormals(self, intersections, mask, normals):
    reshapedMask = mask.reshape([HEIGHT,WIDTH,1])
    n = np.subtract(intersections, self.center)
    n = normalize2dArrayOfVectors(n)
    np.copyto(normals, n, where=reshapedMask)

  def intersects(self, origins, rays, mask, distances):
    cRelative = self.center - origins

    self.writeDistances(cRelative, rays, mask, distances)
# filter out spheres that are behind the camera
    np.logical_and(mask, distances > 1e-4, out=mask)




